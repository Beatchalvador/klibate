import React, { Component } from 'react';
import axios from 'axios';

import Home from './components/Home';
import SearchBar from './components/SearchBar';
import SearchList from './components/SearchList';
import AllBreed from './components/AllBreed'
import AboutUs from './components/AbouUs'

import 'semantic-ui-css/semantic.css';
import { Container } from 'semantic-ui-react';

class App extends Component {
  state = {
    image: undefined,
    breeds: undefined,
    breeds_found: undefined,
    errors: undefined,
    homeVisible: true,
    searchListVisible: false,
    allBreedVisible: false,
    allDogs: undefined,
    breed: undefined,
    dog_with_subreed: undefined,
    subBreed: undefined,
    textInput: undefined,
  }
  componentWillMount(){
    const { breed } = this.state
    this.getBreeds()
    !breed && this.getHomeImage()
    this.getDogsWithSubreed()
  }
  getBreeds = () => {
    axios.get('https://dog.ceo/api/breeds/list')
    .then((response) => this.setState((state) => ({
      breeds: response.data.message,
      })
      )
    )
    .catch((response) => {
      this.setState((state) => ({
        errors: response.message
      })
      )
    })
  }

  setHomeVisible = () => {
    this.setState(state => ({
      homeVisible: true,
      searchListVisible: false,
      allBreedVisible: false,
    }))
  }

  setSearchListVisible = () => {
    this.setState(state => ({
      homeVisible: false,
      searchListVisible: true,
      allBreedVisible: false,
    }))
  }

  setAllBreedVisible = () => {
    this.setState(state => ({
      homeVisible: false,
      searchListVisible: false,
      allBreedVisible: true,
    }), () => this.getAllDogs())
    
  }
  filterBreeds = (text) => {
    const { breeds } = this.state

    const breeds_found = breeds.filter(breed => breed.toLowerCase().includes(text.toLowerCase()))

    this.setState((state) => ({
      breeds_found: breeds_found
    }))
  }

  setRandomBreed(breed){
    
  }
  getHomeImage = () => {
    this.resetTextInput()
    const {  breed, subBreed } = this.state
    let query = '';

    if (breed) {
      query = `https://dog.ceo/api/breed/${breed}/images/random`
      }
    else if (breed && subBreed){
      query = `https://dog.ceo/api/breed/${breed}/${subBreed}/images/random`
    } 
    else {
      query = 'https://dog.ceo/api/breeds/image/random'
    }
    axios.get(query)
      .then((response) => {
        this.setState((state) => ({
          image: response.data.message
        }))
      })
      .catch((response) => {
        this.setState((state) => ({
          errors: response.message
        }))
      })

  } 

  setBreed = (breed) => {
    this.setState(state => {
      return {
        breed: breed,
        subBreed: undefined
      }
    }, () => { 
      this.getHomeImage()
      this.setHomeVisible()})
    
    
  }

  getAllDogs = () => {
    axios.get("https://dog.ceo/api/breeds/list/all")
    .then((response) => {
      this.setState(state => ({
        allDogs: response.data.message
      })
      )

    })
    .catch(response => {
      console.log(response.message)
    })
  }

  getDogsWithSubreed = () => {
    const query = 'https://dog.ceo/api/breeds/list/all'

    axios.get(query)
    .then(response => {
      const allDogs = response.data.message
      const arrayAllDogs = allDogs ? Object.keys(allDogs) : undefined
      const dog_with_subreed = arrayAllDogs.filter(dog => allDogs[dog].length > 0)
      this.setState(state => ({
        dog_with_subreed: dog_with_subreed
      }))
    })
    .catch(response => {
      this.setState(state => ({
        errors: response.message
      }))
    })
  }

  getSubBreed = (breed, subBreed) => {
    this.setState(state => ({
      breed: breed,
      subBreed: subBreed
    }), () => {
      this.getHomeImage()
      this.setHomeVisible()
    })
  }

  onHomeClick = () => {
    this.setState(state => ({
      breed: undefined,
      subBreed: undefined
    }), () => {
      this.getHomeImage()
      this.setHomeVisible()
    })
  }
  resetTextInput = () => {
    this.setState(state => ({
      textInput: ''
    }))
  }
  setTextInput = (text) => {
    this.setState(state => ({
      textInput: text
    }))
  }
  render() {
    const { breeds_found, breeds, homeVisible, searchListVisible, image, allBreedVisible, allDogs, dog_with_subreed, breed, subBreed, textInput} = this.state
    return (
      <Container>
        <SearchBar 
        onHomeClick={this.onHomeClick}
        setAllBreedVisible={this.setAllBreedVisible}
        setHomeVisible={this.setHomeVisible}
        setSearchListVisible={this.setSearchListVisible}
        breeds={breeds}
        filterBreeds={this.filterBreeds}
        setTextInput={this.setTextInput}
        textInput={textInput}
        />
        {homeVisible && <Home
          breed = {breed}
          subBreed = {subBreed}
          image={image}
          getHomeImage={this.getHomeImage}
          />}
        {searchListVisible && <SearchList 
          setBreed={this.setBreed}
          breeds_found={breeds_found} 
          setHomeVisible={this.setHomeVisible}
          textInput={textInput}
          />}
        {allBreedVisible && <AllBreed
          setBreed={this.setBreed}
          getSubBreed={this.getSubBreed}
          dog_with_subreed={dog_with_subreed}
          allDogs={allDogs} />}
        <AboutUs />
      </Container>
    );
  }
}

export default App;
