import React from 'react'
import { Container, Input, Button } from 'semantic-ui-react';

class SearchBar extends React.Component{
    
    onTextChange = (e) => {
        const value = e.target.value
        if (value.length === 0){
            this.props.setHomeVisible()
        }
        else{
            this.props.setSearchListVisible()
            this.props.filterBreeds(value)
        }
        this.props.setTextInput(value)
    }

    onButtonClick = () =>{
        this.props.setAllBreedVisible()
    }
    onHomeClick = () => {
        this.props.onHomeClick()
    }
    render(){
        const { textInput } = this.props
        return(
        <Container style={{'paddingTop': '30px', 'paddingBottom': '10px' }}>
            <Input 
            fluid
            placeholder='Search for breed...'
            value={textInput} 
            onChange={this.onTextChange} >
                <input style={{ 'borderRadius': 0 }} />
                <Button
                    onClick={this.onButtonClick}
                    type='button'
                    style={{ 'borderRadius': 0 }}
                >
                    All Dogs
                </Button>
                <Button
                    type='button'
                    onClick={this.onHomeClick}
                    style={{ 'borderRadius': 0 }}
                    >
                    Random
                </Button>
            </Input>
            
        </Container>
        )
    }
}

export default SearchBar;