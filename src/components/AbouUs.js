import React from 'react';
import { Container, Segment, Grid, Header, List } from 'semantic-ui-react';

const AboutUs = () => {
    return(
        <Container style={{'paddingTop':'10px'}}>
            <Segment inverted>
            <Grid inverted>
                <Grid.Row>
                    <Grid.Column width={8}>
                        <Header inverted as='h4'>Contact Us</Header>
                        <List inverted>
                            <List.Item>alami.hamza.go@gmail.com</List.Item>
                            <List.Item>elhajjajihamza@gmail.com </List.Item>
                        </List>
                    </Grid.Column>
                    <Grid.Column width={8}>
                        <Header inverted as='h4'>Service</Header>
                        <List>
                        <List.Item>
                            Had application dernaha bach tmte3 l3winate w t9oll soubhana lah li khla9 had lklibat.
                        </List.Item>
                        </List>
                    </Grid.Column>
                </Grid.Row>
            </Grid>
            </Segment>
        </Container>
    )
}

export default AboutUs;