import React from 'react';
import { Container, Dimmer, Header, Image, Button, Icon, Segment, Divider, Loader } from 'semantic-ui-react';

class Home extends React.Component {

    onButtonClick = () =>{
        this.props.getHomeImage()
    }
    
    render() {
        const {  breed, subBreed, image, } = this.props
    return(
    <Container>
            <Segment style={{ 'marginBottom': '0px', 'borderRadius':0}}>
        {!breed && <Header as='h1' textAlign='center'>
            Klibate
        </Header>}
        
        <Header as='h2' textAlign='center'>
            {breed &&<span>{breed.toUpperCase()} </span>}
            {subBreed && <span>--  {subBreed.toUpperCase()}</span>}
        </Header>
        
        <Divider />
        { image ?
        <Image 
        onClick={this.onButtonClick}
        size='large' 
        src={image} alt="can't Be load" centered/> : <Dimmer inverted active><Loader /></Dimmer>
        }
        </Segment>
        <Button fluid animated type='button' onClick={this.onButtonClick}
        style={{ 'borderRadius': 0 }}
        >
            <Button.Content visible>
                Wahdakhour
            </Button.Content>

            <Button.Content hidden>
                <Icon name='random'/>
            </Button.Content>
        
        </Button>
        
    </Container>
    )
};
}
export default Home;