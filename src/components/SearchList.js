import React from 'react';
import { Container,Button, Header, Grid, Divider } from 'semantic-ui-react';

class  SearchList  extends React.Component {
    onButtonClick = (breed) => {
        this.props.setBreed(breed)
    }
    render(){
    const { breeds_found, textInput } = this.props
    return (
        <Container>
            <Header as='h1' textAlign='center'>
                List all breeds that contain "{textInput}"
            </Header>
            <Divider />
            {breeds_found.length>0 ? (
                <Grid>                
                    {breeds_found.map(breed => (
                        <Grid.Column key={breed} computer={4} mobile={8} tablet={8}>
                            <Button fluid onClick={() => this.onButtonClick(breed)}>{breed}</Button>
                            </Grid.Column>)
                    )}
                </Grid>) : <h3>Breeds Not Found</h3> }
            
        </Container>
    )
    }
}

export default SearchList;