import React from 'react';
import { Container, Grid, Button, Header, Divider, Label } from 'semantic-ui-react';
class AllBreed extends React.Component {
state = {
    subBreedVisible: false,
    breed: undefined,
    subBreeds: undefined
}
onButtonClick = (dog) => {
    const { dog_with_subreed, allDogs } = this.props
    if (dog_with_subreed.includes(dog)){
        this.setState(state => ({
            breed: dog,
            subBreeds: allDogs[dog],
            subBreedVisible: true
        }))
    }
    else {
        this.props.setBreed(dog)
    }
}
onSubSearch = (subBreed) => {
    const {breed} = this.state
    this.props.getSubBreed(breed, subBreed)
}
onBreedSearch = (breed) => {
    this.props.setBreed(breed)
}
render(){
    const { allDogs } = this.props
    const { breed, subBreeds, subBreedVisible } = this.state
    const firstList = allDogs ? Object.keys(allDogs) : undefined
    return (
    <Container style={{'paddingTop':'10px'}}>
            { !subBreedVisible && (
            <Container>
                <Header as='h1' textAlign='center'>
                        All Breeds
                </Header>
                <Grid>
                { firstList && firstList.map(dog => (
                    <Grid.Column 
                    computer={4} 
                    mobile = {8} 
                    tablet = {8} 
                    key={dog}>
                    <Button fluid type='button' onClick={() => {
                        this.onButtonClick(dog)
                    }}>{dog}</Button>
                    </Grid.Column>
                    ))
                
                }
                
                </Grid>
            </Container>)
            }
            {
            subBreedVisible && 
            (
            <Container>
                <Header as='h1'
                textAlign='center'
                onClick={() => this.onBreedSearch(breed)}>
                    Breed Name: <Label as='a' size='massive' color='black'>{breed}</Label>
                </Header>
                    <Divider horizontal>Sub {breed}</Divider>
                <Grid>
                    {subBreeds && subBreeds.map(subBreed => (
                        <Grid.Column key={subBreed} computer={8} mobile={8} tablet={8}>
                            <Button fluid onClick={() => this.onSubSearch(subBreed)}>
                                {subBreed}
                            </Button>
                        </Grid.Column>
                    )) }
                </Grid>
            </Container>
            )
            }
    </Container>
    )
    }
}

export default AllBreed